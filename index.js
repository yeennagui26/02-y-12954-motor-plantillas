const express = require("express");
const db = require("./db/data");
const hbs = require ("hbs");
const app = express();



app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

app.get("/", (req, res) => {
    res.render("index",{integrantes: db.integrantes,});
});

const matriculas = [...new Set(db.tipomedia.map(item => item.matricula))];
app.get("/:matricula", (request, response, next) => {
    const matricula = request.params.matricula;
    console.log(matricula)
    if (matriculas.includes(matricula)) {
        const tipomediaFiltrada = db.tipomedia.filter(item => item.matricula === matricula);
        const integrantesFiltrados = db.integrantes.filter(item => item.matricula === matricula);
        response.render('integrante', {
            media: db.media,
            integrantes: integrantesFiltrados,
            tipomedia: tipomediaFiltrada,
        });
    }else{
        next();
    }
});
app.use((req, res,) => {
    res.status(404).render('error');
});  

app.listen(3000, () => {
    console.log("Servidor coriendo en el puerto 3000");
    console.log("http://localhost:3000/");
});
    
//console.log ("base de datos simulada", db);
//console.log (db.integrantes);